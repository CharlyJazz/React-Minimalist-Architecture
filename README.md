# React with Server Rendering and Knex

## Introduction

Project developed with the intention of defining a minimalist architectural solution for the creation of a project in React with rendering from the server.

## Features

* Redis for session authentication
* Express Server
* Knex for SQL database management connection
* Redux in the server and client

## Run!

``` bash
$ npm install && npm run db:setup & npm run dev
```
