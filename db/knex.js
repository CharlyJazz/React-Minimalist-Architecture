import config from '../knexfile'
import knex from 'knex'

const currentConfig = config[process.env.NODE_ENV || 'development']

export default knex(currentConfig)
