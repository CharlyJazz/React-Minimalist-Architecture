import knex from '../../knex'

export default {
  findAll: () => {
    return knex.raw(`SELECT * FROM users`)
      .then(data => {
        return data
      })
      .catch(err => console.log(err))
  }
}
