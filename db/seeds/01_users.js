const bcrypt = require('bcryptjs')

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(function () {
      // Create password
      const salt = bcrypt.genSaltSync()
      const hash = bcrypt.hashSync('1234', salt)
      // Inserts seed entries
      return knex('users').insert([
        { id: 1, name: 'Drake', hobbie: 'Sausage things', username: 'drake_666', password: hash },
        { id: 2, name: 'Josh', hobbie: 'Sleep', username: 'josh_666', password: hash },
        { id: 3, name: 'Megan', hobbie: 'Kill old people', username: 'megan_666', password: hash }
      ])
    })
}
