import App from './App'
import Home from './pages/Home'
import UserList from './pages/UserList'
import NotFound from './pages/NotFound'
import Login from './pages/Login'
import AdminList from './pages/AdminList'

export default [
  {
    ...App,
    routes: [
      {
        ...Home,
        path: '/',
        exact: true
      },
      {
        ...UserList,
        path: '/users',
        serverReducers: true
      },
      {
        ...AdminList,
        path: '/admins',
        serverReducers: true
      },
      {
        ...Login,
        path: '/login'
      },
      {
        ...NotFound
      }
    ]
  }
]
