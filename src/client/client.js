import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'

import arrayRoutes from './Routes'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import reducers from './reducers'
import { applyMiddleware, createStore } from 'redux'
import { renderRoutes } from 'react-router-config'
import axios from 'axios'

const axiosInstance = axios.create({
  baseURL: '/api'
})

const store = createStore(reducers, window.INITIAL_STATE, applyMiddleware(thunk.withExtraArgument(axiosInstance)))

delete window.INITIAL_STATE

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <div>
        {renderRoutes(arrayRoutes)}
      </div>
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
)
