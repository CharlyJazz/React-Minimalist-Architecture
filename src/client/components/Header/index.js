import React from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import './styles.scss'

const Header = ({ auth }) => {
  return (
    <div className='Header'>
      <div>
        <Link to='/'>
          SSR Application
        </Link>
      </div>
      <div>
        <Link to='/users'>
          Users
        </Link>
      </div>
      <div>
        <Link to='/admins'>
          Admins
        </Link>
      </div>
      <div>
        {!auth ? <Link to='/login'>Login</Link> : <a href='/logout'> Log out </a>}
      </div>
      {
        auth && (
          <div>
            <strong>
              {auth.username} 🍔
            </strong>
          </div>
        )
      }
    </div >
  )
}

export default connect(({ auth }) => ({ auth }), null)(Header)
