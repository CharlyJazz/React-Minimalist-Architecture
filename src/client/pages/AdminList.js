import { connect } from 'react-redux'

import React, { Component } from 'react'
import { fetchAdmins } from '../actions'
import requireAuth from '../hocs/requireAuth'
import { Helmet } from 'react-helmet'

class AdminList extends Component {
  componentDidMount () {
    if (!this.props.admins.length) {
      this.props.fetchAdmins()
    }
  }

  renderAdmins () {
    return this.props.admins.map(admin => {
      return <li key={admin.id}>{admin.name}</li>
    })
  }

  render () {
    return (
      <div>
        <Helmet>
          <title>Admins App</title>
          <meta property='og:type' content='Admins App' />
        </Helmet>
        <h1>Secret Admins:</h1>
        <ul>
          {this.renderAdmins()}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    admins: state.admins
  }
}

export default {
  component: connect(mapStateToProps, { fetchAdmins })(requireAuth(AdminList)),
  loadData: ({ dispatch }) => dispatch(fetchAdmins())
}
