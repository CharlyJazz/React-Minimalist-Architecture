import { connect } from 'react-redux'

import React, { Component } from 'react'
import { fetchUsers } from '../actions'

class UserList extends Component {
  componentDidMount() {
    if (!this.props.users.length) {
      this.props.fetchUsers()
    }
  }

  renderUsers() {
    return this.props.users.map(user => {
      return <li key={user.id}>{user.name}</li>
    })
  }

  render() {
    return (
      <div>
        <h1>Users:</h1>
        <ul>
          {this.renderUsers()}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users
  }
}

export default {
  component: connect(mapStateToProps, { fetchUsers })(UserList),
  loadData: ({ dispatch }) => dispatch(fetchUsers())
}
