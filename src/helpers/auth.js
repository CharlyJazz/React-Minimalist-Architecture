import bcrypt from 'bcryptjs'

export default function comparePass (userPassword, databasePassword) {
  return bcrypt.compareSync(userPassword, databasePassword)
}
