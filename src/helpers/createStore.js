import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import reducers from '../client/reducers'
import axios from 'axios'

export default (req, initialState = {}) => {
  const axiosInstance = axios.create()

  return createStore(reducers, initialState, applyMiddleware(thunk.withExtraArgument(axiosInstance)))
}
