import express from 'express'
import renderer from './helpers/renderer'
import createStore from './helpers/createStore'
import { matchRoutes } from 'react-router-config'
import arrayRoutes from './client/Routes'
import serverReducer from './server_reducer'
import Users from '../db/models/Users'
import knex from '../db/knex'
import passport from 'passport'
import { Strategy } from 'passport-local'
import redis from 'redis'
import session from 'express-session'
import connectRedis from 'connect-redis'
import bodyParser from 'body-parser'
import cookieParser from 'cookie-parser'
import comparePass from './helpers/auth'

// Configure the local strategy for use by Passport.
//
// The local strategy require a `verify` function which receives the credentials
// (`username` and `password`) submitted by the user.  The function must verify
// that the password is correct and then invoke `cb` with a user object, which
// will be set at `req.user` in route handlers after authentication.

const options = {}

passport.use(new Strategy(options, (username, password, done) => {
  knex('users').where({ username }).first()
    .then((user) => {
      if (!user) return done(null, false)
      if (!comparePass(password, user.password)) {
        return done(null, false)
      } else {
        return done(null, user)
      }
    })
    .catch((err) => { return done(err) })
}))

// Configure Passport authenticated session persistence.
//
// In order to restore authentication state across HTTP requests, Passport needs
// to serialize users into and deserialize users out of the session.  The
// typical implementation of this is as simple as supplying the user ID when
// serializing, and querying the user record by ID from the database when
// deserializing.
passport.serializeUser((user, done) => { done(null, user.id) })

passport.deserializeUser((id, done) => {
  return knex('users').where({ id }).first()
    .then((user) => { done(null, user) })
    .catch((err) => { done(err, null) })
})

const RedisStore = connectRedis(session)
const client = redis.createClient()
const app = express()

app.disable('x-powered-by')

app.use(session({
  secret: 'Sex in the beach',
  store: new RedisStore({ host: 'localhost', port: 6379, client: client, ttl: 260 }),
  saveUninitialized: false,
  resave: false
}))

app.use(passport.initialize())

app.use(passport.session())

app.use(express.static('public'))

app.use(bodyParser.urlencoded({ extended: true }))

app.use(cookieParser())

app.get('/api/users', async (_, res) => {
  const users = await Users.findAll()

  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(users))
})

app.get('/api/admins', async (_, res) => {
  const admins = await Users.findAll() // Mock secret data

  res.setHeader('Content-Type', 'application/json')
  res.send(JSON.stringify(admins))
})

app.post('/login', passport.authenticate('local', {
  successRedirect: '/users',
  failureRedirect: '/login'
}), function (req, res) {
  res.redirect('/')
})

app.get('/logout', function (req, res) {
  req.logout()
  req.session.destroy(function (err) {
    console.log(err) // This no destroy the session in the browser
  })
  res.redirect('/')
})

app.get('*', async (req, res) => {
  let store = null

  if (req.user) {
    delete req.user.password
    store = createStore(req, { auth: req.user })
  } else {
    store = createStore(req, { auth: false })
  }

  const promises = matchRoutes(arrayRoutes, req.path).map(({ route }) => {
    if (route.serverReducers) {
      return serverReducer(req.path)(store)
    } else {
      return route.loadData ? route.loadData(store) : null
    }
  })

  Promise.all(promises).then(() => {
    const context = {}
    const content = (renderer(req, store, context))

    if (context.notFound === true) {
      res.status(404)
    }

    res.send(content)
  })
})

app.listen(3000, () => {
  console.log('Listening on port 3000')
})
