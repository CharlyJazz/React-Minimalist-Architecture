import Users from '../../db/models/Users'
import { FETCH_USERS, FETCH_ADMINS } from '../client/actions'

const fetchUsers = () => async (dispatch, getState, api) => {
  const res = await Users.findAll()

  dispatch({
    type: FETCH_USERS,
    payload: { data: res }
  })
}

export const fetchAdmins = () => async (dispatch, getState, api) => {
  const res = await Users.findAll()

  dispatch({
    type: FETCH_ADMINS,
    payload: { data: res }
  })
}

export default (path) => {
  switch (path) {
    case '/users':
      return ({ dispatch }) => dispatch(fetchUsers())
    case '/admins':
      return ({ dispatch }) => dispatch(fetchAdmins())
    default:
      return null
  }
}
