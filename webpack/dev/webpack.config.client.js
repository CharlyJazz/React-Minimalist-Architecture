const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const config = {
  mode: 'development',
  entry: path.resolve(__dirname, '../../src/client/client.js'),
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../../public')
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(scss|css)$/i,
        use: [
          {
            loader: MiniCssExtractPlugin.loader
          },
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles.css',
      chunkFilename: '[id].css'
    })
  ],
  node: {
    fs: 'empty'
  }
}

module.exports = config
