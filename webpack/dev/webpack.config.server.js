const path = require('path')
const nodeExternals = require('webpack-node-externals')
const config = {
  mode: 'development',
  target: 'node',
  externals: [nodeExternals()],
  entry: path.resolve(__dirname, '../../src/index.js'),
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../../build')
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(scss|css)$/i,
        use: [
          'css-loader',
          'sass-loader'
        ]
      }
    ]
  }
}

module.exports = config
